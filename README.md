# Tegra Builder

## Usage

Run the container and build `fedora` distribution for `icosa` platform :
```
docker run --rm -it --privileged -e DISTRO=fedora -e PLATFORM=icosa -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd):/out 
```

Or if you have qemu static and binfmt you can run the script manually :
```sh
DISTRO=hirsute PLATFORM=icosa ./build.sh
```

## Contributors

@nicman23
