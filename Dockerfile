FROM ubuntu:18.04

ENV DISTRO PLATFORM

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt install -y \
	qemu \
	qemu-user-static \
	binfmt-support \
	p7zip-full \
	util-linux \
	zerofree \
	git \
	libguestfs-tools \
	docker.io \
	tar \
	wget \
	xxd \
	linux-image-generic || true \
	&& rm -rf /var/lib/apt/lists/*

VOLUME /out

WORKDIR /build
COPY . /build/

CMD /build/build.sh && \
	cp /build/${PLATFORM}-${DISTRO}-* /out
