#!/bin/bash
set -e

# Export BSP URL
export T21X_BSP="https://developer.nvidia.com/embedded/l4t/r32_release_v5.1/r32_release_v5.1/t210/jetson-210_linux_r32.5.1_aarch64.tbz2"
export T18X_BSP="https://developer.nvidia.com/embedded/l4t/r32_release_v5.1/r32_release_v5.1/t186/tegra186_linux_r32.5.1_aarch64.tbz2"

# PLATFORM validation
case ${PLATFORM} in
	agx)
		export BOARD="jetson-agx-xavier-devkit" \
                        TEGRA_ID="0x19" PLATFORM="xavier" \
			BSP=${T18X_BSP};;
	agx-8gb)
                export BOARD="jetson-agx-xavier-devkit-8GB" \
                        TEGRA_ID="0x19" PLATFORM="xavier" \
                        BSP=${T18X_BSP};;
	icosa)
		export PLATFORM="icosa";;
	tx1)
		export BOARD="jetson-tx1-devkit" \
			TEGRA_ID="0x21" PLATFORM="porg" \
                        BSP=${T21X_BSP};;
	tx2)
		export BOARD="jetson-tx2-devkit" \
			TEGRA_ID="0x18" PLATFORM="tx2" \
                        BSP=${T18X_BSP};;
	tx2-4gb)
                export BOARD="jetson-tx2-devkit-4GB" \
                        TEGRA_ID="0x18" PLATFORM="tx2" \
                        BSP=${T18X_BSP};;
	tx2i)
		export BOARD="jetson-tx2i-devkit" \
                        TEGRA_ID="0x18" PLATFORM="tx2" \
                        BSP=${T18X_BSP};;
	nano)
		export BOARD="jetson-nano-devkit" \
			TEGRA_ID="0x21" PLATFORM="porg" \
                        BSP=${T21X_BSP};;
	nano-2gb)
		export BOARD="jetson-nano-2gb-devkit -r 200" \
			TEGRA_ID="0x21" PLATFORM="porg" \
                        BSP=${T21X_BSP};;
        xavier)
                export BOARD="jetson-xavier-nx-devkit" \
			TEGRA_ID="0x19" PLATFORM="xavier" \
                        BSP=${T18X_BSP};;
        *)
                printf "\e[31mUnknown Jetson nano board type\e[0m\n"
                exit;;
esac

printf "Creating ${DISTRO} image for ${PLATFORM} board"

# Export Docker variables
export DOCKER_BUILDKIT=1 buildkit_progress=plain COMPOSE_DOCKER_CLI_BUILD=1

# Export image name
export NAME="${PLATFORM}-${DISTRO}-$(date '+%Y-%m-%d')"

# Register QEMU binfmt
docker run --rm --privileged aptman/qus -s -- -c -p || true

# Build image
docker build -f ${PLATFORM}/Dockerfile.${DISTRO} -t l4t-${DISTRO} ${PLATFORM}

# Export image as tar
docker export -o "l4t-${DISTRO}.tar" `docker run -d l4t-${DISTRO} /bin/true`

# Remove container afterwards
docker rmi -f `docker images -q l4t-${DISTRO}`

if [[ ${PLATFORM} == "icosa" ]]; then
	# Create disk image
	virt-make-fs l4t-${DISTRO}.tar "${NAME}.img" --size=+768M -t ${TYPE:-ext4} --label "SWR-$(echo ${DISTRO:0:3} | tr '[:lower:]' '[:upper:]')"

	# Zerofree the image 
	zerofree -n "${NAME}.img"

	if [[ -n ${BOOT} ]]; then
		# Clone switchroot bootstack
		git clone https://gitlab.com/switchroot/bootstack/bootstack-build-scripts

		# Create bootstack using NO_COMPRESS option
		NO_COMPRESS=true ./bootstack-build-scripts/build.sh .

		# Get build directory size
		size="$(du -b -s "${NAME}.img" | awk '{print int($1);}')"

		# Alignement adjust to 4MB
		aligned_size=$(((${size} + (4194304-1)) & ~(4194304-1)))

		# Check if image needs alignement
		align_check=$((${aligned_size} - ${size}))

		# Align diskimage if necessary
		if [[ ${align_check} -ne 0 ]]; then
			dd if=/dev/zero bs=1 count=${align_check} >> "${NAME}.img"
		fi

		# Create hekate boot dirs
		mkdir -p switchroot/install/ bootloader/

		# Split into 4GB part
		split -b4290772992 --numeric-suffixes=0 "${NAME}.img" switchroot/install/l4t.

		# Finally create 7zip
		7z a "${NAME}.7z" ./switchroot ./bootloader

		# Clean diskimage to avoid copying it in docker
		rm -rf "${NAME}.img"
	fi
else
	echo "Using BSP to create jetson disk image."

	# Download and extract BSP
	wget -qO- $BSP | tar -jxpf -

	# Remove useless README
	rm Linux_for_Tegra/rootfs/README.txt

	# Extract build
	tar xf "l4t-${DISTRO}.tar" -C Linux_for_Tegra/rootfs/

	# losetup --find
	
	cd Linux_for_Tegra/

	# Create jetson flashable image
	./flash.sh --no-root-check --no-flash -x ${TEGRA_ID} ${BOARD} mmcblk0p1

	cd ../

	# Rename image correctly
	mv Linux_for_Tegra/system.img ${NAME}.img
fi
